OLDIFS=$IFS
IFS=","

config_file="/src/config.1000.size300.layers2.batch5000.json"
data_file="/src/xpos_rep2_gpu.csv"
task="xtag"
exp_prefix="${task}_1000_s300_l2"

while read lang_abbr ud_dir embeddings_abr embeddings_dir; 
do
  #echo "$line"
  echo "$lang_abbr $ud_dir $embeddings_abr $embeddings_dir"
  mkdir "/output/${lang_abbr}_${exp_prefix}"
  tar xzvf /input/ud-treebanks-conll2017.tgz -C /input --wildcards "ud-treebanks-conll2017/UD_${ud_dir}/${lang_abbr}-ud-*.conllu"
  tar xvf /input/word-embeddings-conll17.tar -C /input --wildcards "${embeddings_dir}/${embeddings_abr}.vectors.xz"
  tar xzvf /input/ud-test-v2.0-conll2017.tgz -C /input --wildcards "ud-test-v2.0-conll2017/input/conll17-ud-test-2017-05-09/${lang_abbr}-udpipe.conllu"
  tar xzvf /input/ud-test-v2.0-conll2017.tgz -C /input --wildcards "ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/${lang_abbr}.conllu"
  xz -d "/input/${embeddings_dir}/${embeddings_abr}.vectors.xz"
  python2 /src/train_cw.py --train="/input/ud-treebanks-conll2017/UD_${ud_dir}/${lang_abbr}-ud-train.conllu" --dev="/input/ud-treebanks-conll2017/UD_${ud_dir}/${lang_abbr}-ud-dev.conllu" --embeddings="/input/${embeddings_dir}/${embeddings_abr}.vectors" --task="${task}" --config="$config_file" --output_dir="/output/${lang_abbr}_${exp_prefix}/" > "/output/train_${lang_abbr}_${exp_prefix}.txt"
  python2 /src/test_cw.py --test="/input/ud-test-v2.0-conll2017/input/conll17-ud-test-2017-05-09/${lang_abbr}-udpipe.conllu" --task="${task}" --output_dir="/output/${lang_abbr}_${exp_prefix}/" --out="/output/datasets/${exp_prefix}_${lang_abbr}-udpipe-pred.conllu"  --config="$config_file" > "/output/test_cw_${lang_abbr}-udpipe_${exp_prefix}_output.txt"
  python /src/evaluation/conll17_ud_eval.py -v "/input/ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/${lang_abbr}.conllu" "/output/datasets/${exp_prefix}_${lang_abbr}-udpipe-pred.conllu" > "/output/tables_and_plots/${exp_prefix}_${lang_abbr}-udpipe-pred.eval.txt"
  rm "/input/${embeddings_dir}/${embeddings_abr}.vectors"
done < "$data_file"

IFS=$OLDIFS
