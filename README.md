# repro_meta_tagger

Reproduction of:

Bernd Bohnet, Ryan McDonald, Gonçalo Simões, Daniel Andor, Emily Pitler, Joshua 
Maynez. Morphosyntactic Tagging with a Meta-BiLSTM Model over Context Sensitive 
Token Encodings. ACL, 2018.

Our reproduction uses the original implementation of their model with some 
slight adjusments. The original source code was downloaded from: 
https://github.com/google/meta_tagger

This is one of 8 tags of this repository that produce part of the results of 
a study done in the context of the REPROLANG 2020 shared task:

Yung Han Khoe. Reproducing a Morphosyntactic Tagger with a Meta-BiLSTM Model over 
Context Sensitive Token Encodings. 2019

Each reproduced result in Table 1 and 2 of the reproducion paper will be 
represented by one file in the `/output/tables_and_plots` directory. These 
files are named as: 
`<task>_<model configuration>_<data_set abbreviation>-udpipe-pred.eval.txt`. 
The task is either part-of-speech tagging (`xtag`), or tagging of morphological 
features (`feats`).
The model configuration is either `1000_s400_l3` for the first replication or 
`1000_s300_l2` for the second replication.
In sum, a file named `feats_1000_s300_l2_ar-udpipd-pred.eval.txt` contains the 
result of the second reproduction (Rep2) on tagging of morphological features 
on the Arabic data set.
The results are either in the `XPOS` or the `Feats` row, and in the `F1 score` 
column of those files.

Each tag runs either the part-of-speech tagging task or the morphological 
features task, with the configuration of either our first or our second 
replication, either with or without using a gpu.

This specific branch of the docker image produces results on the part-of-speech 
tagging task that were produced using the configuration from the repository
(Rep2) using a gpu.
The TensorFlow docker image on which it is based runs TensorFlow version 1.14.0, 
which requires CUDA 9.0.  

The experiments require three publicly available data sets to be present in the
`/input` directory:
* ud-test-v2.0-conll2017.tgz (Development and test data), MD5: c084cbd8733c52d6b9a08a1f5ffa5a28, https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-2184/ud-test-v2.0-conll2017.tgz
* word-embeddings-conll17.tar (Word embeddings), MD5: 8cd278935969de9a9a4f7839b0d6a5e7, https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-1989/word-embeddings-conll17.tar
* ud-treebanks-conll2017.tgz (Training and development data), MD5: f4869f28c376c360c740ef8caafdfffd, https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-1983/ud-treebanks-conll2017.tgz

Run this command to build the experiment locally:
```
cd "${HOME}/${EXPERIMENT}"	#Make sure you are in the root of the project dir
bash build.sh --build --local

```

Run this command to run the experiment locally:

```
cd  ${HOME}/${EXPERIMENT}	#Make sure you are in the root of the project dir
docker run  --gpus all
     -ti --rm --name=$PROJECT_NAME
     -v ${PWD}/input:/input
     -v ${PWD}/src:/src
     -v ${PWD}/output:/output
     -v ${PWD}/output/datasets:/output/datasets
     -v ${PWD}/output/tables_and_plots:/output/tables_and_plots
     <image name>:<image tag>
```     
